package ee.bcs.demo;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class SimpleServiceNumbersTest {

	@Test
	public void testAdd5() {
		SimpleService simpleService = new SimpleService();
		int result = simpleService.add5(5);
		assertEquals(10, result);
		
		result = simpleService.add5(15);
		assertEquals(20, result);
	}
	
	@Test
	public void testMultiplyWith5() {
		SimpleService simpleService = new SimpleService();
		int result = simpleService.multiplyWith5(11);
		assertEquals(55, result);
	}
}
