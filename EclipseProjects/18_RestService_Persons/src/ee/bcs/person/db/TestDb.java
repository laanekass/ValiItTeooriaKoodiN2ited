package ee.bcs.person.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

import com.mysql.cj.api.mysqla.result.Resultset;

public class TestDb {
	public static void main(String[] args) {
		String dbUrl = "jdbc:mysql://localhost:3306/autohaldus";
		Properties connectionProperties = new Properties();
		connectionProperties.put("user", "vali_it");
		connectionProperties.put("password", "Vali_it_!2017");
		try (Connection connection = DriverManager.getConnection(dbUrl, connectionProperties)) {
			String sqlQuery = "SELECT * from auto";
			Statement statement = connection.createStatement();
			ResultSet results = statement.executeQuery(sqlQuery);
			while (results.next()) {
				System.out.println(results.getString("tootja_nimi"));

			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
