package ee.bcs.person;

import java.util.ArrayList;
import java.util.List;

public class Department {
	private static int idCounter = 1;
	private static int departmentId;
	private String departmentName;
	private Employee departmentMager;
	private List<Employee> departmentEmployees = new ArrayList<>();
	
	public Department() {
		super();
		setDepartmentId();
	}

	public Department(String departmentName, Employee departmentMager, List<Employee> departmentEmployees) {
		super();
		setDepartmentId();
		this.departmentName = departmentName;
		this.departmentMager = departmentMager;
		this.departmentEmployees = departmentEmployees;
	}
	
	private static void setDepartmentId(){
		departmentId= idCounter;
		idCounter++;
	}

	public int getDepartmentId() {
		return departmentId;
	}

	public String getDepartmentName() {
		return departmentName;
	}

	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}

	public Employee getDepartmentMager() {
		return departmentMager;
	}

	public void setDepartmentMager(Employee departmentMager) {
		this.departmentMager = departmentMager;
	}

	public List<Employee> getDepartmentEmployees() {
		return departmentEmployees;
	}

	public void setDepartmentEmployees(List<Employee> departmentEmployees) {
		this.departmentEmployees = departmentEmployees;
	}	
}
