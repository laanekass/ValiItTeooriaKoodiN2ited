package ee.bcs.chooseit.visibility;

public class VisibilitySubClassInSamePackage extends Visibility {
	@Override
	public void testVariablesInVisibility() {
		Visibility visibility = new Visibility();
		// System.out.println(visibility.privateClassVariable);
		System.out.println(visibility.defaultClassVariable);
		System.out.println(visibility.protectedClassVariable);
		System.out.println(visibility.publicClassVariable);
	}
}
