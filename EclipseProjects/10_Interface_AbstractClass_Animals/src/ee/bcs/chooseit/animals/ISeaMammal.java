package ee.bcs.chooseit.animals;

public interface ISeaMammal {
    void swimAhead();
    void dive();
}
