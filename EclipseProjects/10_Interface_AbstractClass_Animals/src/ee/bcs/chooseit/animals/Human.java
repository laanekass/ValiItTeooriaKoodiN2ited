package ee.bcs.chooseit.animals;

public class Human extends Mammal implements ILandMammal, ILandMammalWithTwoLegs  {

    boolean isStanding = false;

    @Override
    public void moveAhead() {
        if (!isStanding) {
            standUp();
        }
        makeStepWithLeg(Leg.LEFT);
        makeStepWithLeg(Leg.RIGHT);
    }

    @Override
    public void standUp() {
        System.out.println("Human stood up.");
    }

    @Override
    public void makeStepWithLeg(Leg leg) {
        System.out.println("Human made step ahead with " + leg.toString() +" leg.");
    }
}
