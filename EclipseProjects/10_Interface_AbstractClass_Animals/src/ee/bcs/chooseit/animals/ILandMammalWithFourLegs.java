package ee.bcs.chooseit.animals;

public interface ILandMammalWithFourLegs {
	void makeStepWithDiagonalLegs(Leg frontLeg, Leg backLeg);
}
