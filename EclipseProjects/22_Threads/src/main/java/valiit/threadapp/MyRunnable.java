package valiit.threadapp;

public class MyRunnable implements Runnable{

	private String id = null;
	
	public MyRunnable(String id) {
		this.id = id;
	}
	
	public void run() {
		this.easySleep();
		System.out.println("Runnable runs: " + this.id);
	}

	private void easySleep() {
		try {
			long seconds = (long)(Math.random() * 10 * 1000);
			System.out.println(String.format("Runnable %s: I am going to sleep %s seconds", this.id, seconds / 1000.0));
			Thread.sleep(seconds);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}	
}
