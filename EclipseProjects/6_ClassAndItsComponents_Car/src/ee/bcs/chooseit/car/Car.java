package ee.bcs.chooseit.car;
/**
 * 
 * @author heleen
 *
 */
public class Car {
	private int numberOfSeats;
	private int numberOfDoors;
	private Engine engine;

	public Car() {

	}

	public Car(Engine engine) {
		this.engine = engine;
	}

	public int getNumberOfSeats() {
		return numberOfSeats;
	}

	public void setNumberOfSeats(int numberOfSeats) {
		this.numberOfSeats = numberOfSeats;
	}

	public int getNumberOfDoors() {
		return numberOfDoors;
	}

	public void setNumberOfDoors(int numberOfDoors) {
		this.numberOfDoors = numberOfDoors;
	}

	public Engine getEngine() {
		return engine;
	}

	public void setEngine(Engine engine) {
		this.engine = engine;
	}
}
