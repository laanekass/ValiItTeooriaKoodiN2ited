package ee.bcs.chooseit.car;

/**
 * 
 * @author heleen
 *
 */
public class Engine {
	int power;
	FuelType fuelType;

	public Engine(int power, FuelType fuelType) {
		this.power = power;
		this.fuelType = fuelType;
	}

	public int getPower() {
		return power;
	}

	public void setPower(int power) {
		this.power = power;
	}

	public FuelType getFuelType() {
		return fuelType;
	}

	public void setFuelType(FuelType fuelType) {
		this.fuelType = fuelType;
	}
}
