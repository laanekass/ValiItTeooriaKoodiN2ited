/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ee.bcs.chooseit.employeemanagement.model;

import java.math.BigDecimal;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author heleen
 */
public class Employee implements IEmployee {

    private StringProperty firstName;
    private StringProperty lastName;
    private StringProperty personalID;
    private ObjectProperty<Gender> gender;

    public Employee() throws Exception {
        this(null, null, null);
    }

    public Employee(String firstName, String lastName, String personalID) throws Exception {
        this.firstName = new SimpleStringProperty(firstName);
        this.lastName = new SimpleStringProperty(lastName);
        if (personalID == null) {
            this.personalID = new SimpleStringProperty(personalID);
        } else if (personalID.matches("\\d{11}")) {
            this.personalID = new SimpleStringProperty(personalID);
        } else {
            throw new Exception("Incorrect personalId");
        }

        if (personalID != null) {
            setGenderFromPersonalID();
        }
    }

    public String getFirstName() {
        return this.firstName.get();
    }

    public StringProperty firstNameProperty() {
        return this.firstName;
    }

    public Employee setFirstName(String firstName) {
        this.firstName.set(firstName);
        return this;
    }

    public String getLastName() {
        return this.lastName.get();
    }

    public StringProperty lastNameProperty() {
        return this.lastName;
    }

    public Employee setLastName(String lastName) {
        this.lastName.set(lastName);
        return this;
    }

    public String getPersonalID() {
        return this.personalID.get();
    }

    public StringProperty personalIDProperty() {
        return this.personalID;
    }

    public Employee setPersonalId(String personalId) {
        this.personalID.set(personalId);
        setGenderFromPersonalID();
        return this;
    }

    public Gender getGender() {
        return this.gender.get();
    }

    public ObjectProperty<Gender> genderProperty() {
        return this.gender;
    }

    @Override
    public BigDecimal getSalary() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String toString() {
        return "Employee{" + "firstName=" + firstName + ", lastName=" + lastName + ", personalID=" + personalID + ", gender=" + gender + '}';
    }

    public void setGenderFromPersonalID() {
        this.gender = isMale() ? new SimpleObjectProperty<>(Gender.MALE) : new SimpleObjectProperty<>(Gender.FEMALE);
    }

    private boolean isMale() {
        return this.personalID.get().charAt(0) % 2 != 0;
    }

}
