package ee.bcs.chooseit.collectionsintro;

import java.util.ArrayList;
import java.util.List;

public class ListIntro {
	public static void main(String[] args) {
		List<String> textLog = new ArrayList<>();
		textLog.add("This is first sentence");
		textLog.add("This is second sentence");
		printArray(textLog);
		textLog.add(1, "This is third sentence that is on second position");
		printArray(textLog);
		textLog.add("Fourth sentence goes to end of list");
		textLog.remove(1);
		printArray(textLog);
		System.out.println("List size is " + textLog.size());
		System.out.println("Text on second position is: '" + textLog.get(1) + "'");
		System.out.println("--------------");

		List<Integer> integerListWithInitialSize = new ArrayList<>(5);
		System.out.println("List size is " + integerListWithInitialSize.size());
		integerListWithInitialSize.add(200);
		integerListWithInitialSize.add(300);
		System.out.println("List size is " + integerListWithInitialSize.size());
		for (Integer listElements : integerListWithInitialSize) {
			System.out.println(listElements);
		}
	}

	private static void printArray(List<String> textLog) {
		// same using functional operations (lambda)
		textLog.forEach(textElement -> {
			System.out.println(textElement);
		});
		System.out.println("--------------");
	}
}
