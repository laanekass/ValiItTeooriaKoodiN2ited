package ee.bcs.chooseit;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class StringScanner {
	public static void main(String[] args) {
		String scanneriTekst = "Name:Heleen Maibak Name:Henn Sarv Name:Mari Maasikas";

		// separates by spaces
		Scanner scanner = new Scanner(scanneriTekst);
		while (scanner.hasNext()) {
			System.out.println(scanner.next());
		}
		System.out.println("--------------------------------");
		scanner.close();

		// Separates by given word
		Scanner scannerByWord = new Scanner(scanneriTekst);
		scannerByWord.useDelimiter("Name:");
		while (scannerByWord.hasNext()) {
			System.out.println(scannerByWord.next());
		}
		System.out.println("--------------------------------");
		scannerByWord.close();

		// Reads text from file and separates by word "Name:"
		try (Scanner scannerFile = new Scanner(new File("testFileForScanner2.txt")).useDelimiter("Name:")) {
			while (scannerFile.hasNext()) {
				System.out.println(scannerFile.next());
			}
		} catch (FileNotFoundException ex) {
			Logger.getLogger(StringScanner.class.getName()).log(Level.SEVERE, "Faili ei leitud", ex);
		}
	}
}
