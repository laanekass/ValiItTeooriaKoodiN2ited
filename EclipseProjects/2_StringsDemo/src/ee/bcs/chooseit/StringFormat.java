package ee.bcs.chooseit;

/**
 * 
 * @author heleen
 *
 */
public class StringFormat {
	public static void main(String[] args){
        String stringToFormat = "%s. %s is %s years old. %<s years old is %2$s.";
        System.out.println(String.format(stringToFormat, 1, "Mari", 29));
    }
}
