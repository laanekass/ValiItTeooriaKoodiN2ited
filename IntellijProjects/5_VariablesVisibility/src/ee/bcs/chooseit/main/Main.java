package ee.bcs.chooseit.main;

import ee.bcs.chooseit.visibility.Visibility;
import ee.bcs.chooseit.visibility.Visibility2;
import ee.bcs.chooseit.visibility.VisibilitySubClassInSamePackage;
import ee.bcs.chooseit.visibility.sub.VisibilitySubClassInOtherPackage;

/**
 * Created by heleen on 27.06.2017.
 */
public class Main {
    public void testiVariablesInVisibility() {
        Visibility visibility = new Visibility();
        // System.out.println(visibility.privateClassVariable);
        // System.out.println(visibility.defaultClassVariable);
        // System.out.println(visibility.protectedClassVariable);
        System.out.println(visibility.publicClassVariable);
    }

    public static void main(String[] args) {
        Visibility visibility = new Visibility();
        visibility.testVariablesInVisibility();

        Visibility2 visibility2 = new Visibility2();
        visibility2.testVariablesInVisibility();

        VisibilitySubClassInSamePackage visibilitySubClassInSamePackage = new VisibilitySubClassInSamePackage();
        visibilitySubClassInSamePackage.testVariablesInVisibility();

        VisibilitySubClassInOtherPackage visibilitySubClassInOtherPackage = new VisibilitySubClassInOtherPackage();
        visibilitySubClassInOtherPackage.testVariablesInVisibility();
    }
}
