package ee.bcs.chooseit.animals;

/**
 * Created by heleen on 28.06.2017.
 */
public interface ILandMammal {
    void standUp();
}
