package ee.bcs.chooseit.animals;

/**
 * Abstract class cannot be initiated, it has to be extended by some subclass
 *
 * @author heleen
 */
public abstract class Mammal {

    private String speciesName;
    private Gender gender;

    /**
     * Abstract method that has to be implemented in subclass
     */
    public abstract void moveAhead();

    public String getSpeciesName() {
        return speciesName;
    }

    /**
     * regular method that can be overridden in subclass, but doesn't have to
     *
     * @param speciesName
     * @return
     */
    public Mammal defineSpeciesName(String speciesName) {
        this.speciesName = speciesName;
        return this;
    }

    public Gender getGender() {
        return this.gender;
    }

    public Mammal setGender(Gender gender) {
        this.gender = gender;
        return this;
    }
}
