package ee.bcs.chooseit.threading;

/**
 *
 * @author heleen
 */
public class ExampleThread extends Thread{
    
    @Override
    public void run(){
        for (int i = 0; i<100;i++){
            System.out.println("thread i = " + i);
        }
    }
}
