package ee.bcs.chooseit.car;

/**
 * Created by heleen on 27.06.2017.
 */
public class Engine {
    int power;
    FuelType fuelType;

    public Engine(int power, FuelType fuelType) {
        this.power = power;
        this.fuelType = fuelType;
    }

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }

    public FuelType getFuelType() {
        return fuelType;
    }

    public void setFuelType(FuelType fuelType) {
        this.fuelType = fuelType;
    }
}
