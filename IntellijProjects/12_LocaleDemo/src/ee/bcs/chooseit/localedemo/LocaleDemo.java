/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ee.bcs.chooseit.localedemo;

import java.text.DateFormat;
import java.text.MessageFormat;
import java.text.NumberFormat;
import java.util.Date;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 *
 * @author heleen
 */
public class LocaleDemo {

    Locale eeLocale = new Locale("ee", "EE");
    Locale frLocale = Locale.FRANCE;
    Locale ukLocale = Locale.UK;
    Locale usLocale = Locale.US;
    Locale currentLocale = Locale.getDefault();
    NumberFormat currency;
    DateFormat dateFormat;
    public ResourceBundle messages = ResourceBundle.getBundle("messages", currentLocale);

    public void printHello(String name) {
        System.out.println(MessageFormat.format(messages.getString("helloText"), name));
    }

    public void printBye(String name) {
        System.out.println(MessageFormat.format(messages.getString("byeText"), name));
    }

    public void setLocaleToEstonia() {
        currentLocale = eeLocale;
        messages = ResourceBundle.getBundle("messages", currentLocale);
    }

    public void setLocaleToFrance() {
        currentLocale = frLocale;
        messages = ResourceBundle.getBundle("messages", currentLocale);
    }

    public void setLocaleToUS() {
        currentLocale = usLocale;
        messages = ResourceBundle.getBundle("messages", currentLocale);
    }

    public void setLocaleToDefault() {
        currentLocale = Locale.getDefault();
        messages = ResourceBundle.getBundle("messages", currentLocale);
    }

    public void showDate(Date date) {
        dateFormat = DateFormat.getDateInstance(DateFormat.LONG, currentLocale);
        System.out.println(dateFormat.format(date));
    }

    public void showMoneyWithCurrency(Double money) {
        currency = NumberFormat.getCurrencyInstance(currentLocale);
        System.out.println(currency.format(money));
    }
}
