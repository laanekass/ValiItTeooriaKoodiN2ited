package ee.bcs.chooseit;

import java.util.Arrays;

/**
 * Created by heleen on 26.06.2017.
 */
public class TransformingArrays {
    public static void main(String[] args) {
        String[] array = { "position 0", "position 1", "position 2", "position 3" };
        System.out.println("Printing array straight gives memory address: " + array);
        System.out.println("Printing array after using built in function toString to get String: " + Arrays.toString(array));

        String[][] tabel = { { "row 0 column 1", "row 0 column2", "row 0 column 3" }, { "row 1 column 0", "row 1 column 1" },
                { "row 2 column 0", null, "row 2 column 2" } };
        System.out.println("Printing array straight gives memory address: " + tabel);
        System.out.println("Printing array after using built in function toString to get String: " + Arrays.toString(tabel));
        System.out.println("Printing array after using built in function deepToString to get String" + Arrays.deepToString(tabel));
    }
}
