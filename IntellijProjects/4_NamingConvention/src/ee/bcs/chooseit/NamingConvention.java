package ee.bcs.chooseit;

/**
 * Class names are in UpperCamelCase, typically singular nouns
 *
 * @author heleen
 *
 */
public class NamingConvention {
    private final String UPPER_CASE_VARIABLE_NAME = "Constant name has all characters upper case and words are sepparated by underscore";
    private String lowerCamelCase = "Regular variables names are in lowerCamelCase, where first word starts with lower case, and every following one upper case";

    /**
     * Method names and method parameters are in lowerCamelCase
     * @param camelNames
     */
    private static void writingSleepingCamelsName(String[] camelNames) {
        for (String camelName : camelNames) {
            System.out.println("There is sleeping camel with name " + camelName);
        }
        // following for-loop is equal to previous one, but enables you to take
        // action each separate position
        for (int i = 0; i < camelNames.length; i++) {
            System.out.println("There is sleeping camel with namea " + camelNames[i]);
        }
    }

    /**
     * @param args- the command line arguments
     */
    public static void main(String[] args) {
        NamingConvention namingConvetion = new NamingConvention();
        System.out.println(namingConvetion.UPPER_CASE_VARIABLE_NAME);
        System.out.println(namingConvetion.lowerCamelCase);

        String[] camels = { "Albert", "Maasikas" };
        NamingConvention.writingSleepingCamelsName(camels);
    }
}