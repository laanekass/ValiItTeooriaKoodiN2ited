Here are the examples of code-base for Vali-It training materials. 
Same demos are repeated in Netbeans, Eclipse and Intellij, 
so it is easy to import and run these projects from different IDE-s. 
- 1_SteppingSourceAndComments  
	- shows how source should be intended and how different comment-types can be used
- 2_StringsDemo 
	- Strings in general, comparing Strings, split, format, Scanner, StringBuffer and StringBuilder
- 3_DataTypeTrandsferDemo 
	- transforming simple types, like Integer to String and vice versa; printing arrays (toString and deepToString)
- 4_NamingConventions 
	- some naming practices in java
- 5_Visibility 
	- visiblity demo and testing on class variables example, solution for exercise Muutujate nähtavus
- 6_ClassAndItsComponents_Car 
	- introducing class and it's components
- 7_VarargsDemo 
	- varargs - using unknown number of input values
- 8_CollectionsIntro 
	- collections lists, sets and maps
- 9_Override_polymorphism_Employee 
	- overriding, polymorphis
- 10_Interface_AbstractClass_Animals 
	- Interface, overriding, polymorphism, abstract class and -method
- 11_ExceptionHandling 
	- creating and handling exceptions (throws, try-catch)
- 12_LocaleDemo 
	- creating and using properties file for different languages
- 13_IODemo 
	- reading and writing files
- 14_RegularExpressions 
	- usage of regular expressions
- 15_SerializeDemo 
	- simple serializing example
- 16_Threading 
	- creating threads
- 17_UserInterfaceWithJavaFX_EmployeeManagemnet 
	- User interface creation with JavaFX
- 18_TestRest - Rest Service example
- 19_Vali_IT_Web_application
	- Skeleton project for developing the demo application.
- 20_Vali_IT_UnitTesting_Demo
	- Simple demo for creating unit tests.
19. RestClient - Rest Client using HTML, JavaScript and CSS
20. UnitTestDemo - unit testing