package ee.bcs.chooseit.exceptionhandling;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author heleen
 */
public class ExceptionHandling {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try (FileReader reader = new FileReader(new File("notExistingFile.txt"));
                FileWriter writer = new FileWriter(new File("outputResult.txt"))) {
            char[] fileContent = new char[500];
            reader.read(fileContent);

            int i = 1;
            while (i <= 10) {
                writer.append("Hi for the " + i + ". time \n");
                i++;
            }
            writer.flush();

        } catch (FileNotFoundException ex) {
            Logger.getLogger(ExceptionHandling.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ExceptionHandling.class.getName()).log(Level.SEVERE, null, ex);
        }

        Connection conn = null;
        String dbUrl = "jdbc:mysql://localhost:3306/";
        Properties connectionProps = new Properties();
        connectionProps.put("user", "kasutajaMari");
        connectionProps.put("password", "kasutajaMariParool");
        
        //developer has to close all resources
        try (FileReader reader = new FileReader(new File("notExistingFile.txt"));
                FileWriter writer = new FileWriter(new File("outputResult.txt"))) {
            char[] fileContent = new char[500];
            reader.read(fileContent);

            conn = DriverManager.getConnection(dbUrl, connectionProps);
            int i = 1;
            while (i <= 10) {
                writer.append("Hi for the " + i + ". time \n");
                i++;
            }
            writer.flush();

        } catch (FileNotFoundException ex) {
            Logger.getLogger(ExceptionHandling.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException | SQLException ex) {
            Logger.getLogger(ExceptionHandling.class.getName()).log(Level.SEVERE, null, ex);
        }

        //developer has to close all resources
        try {
            FileWriter writer = new FileWriter(new File("outputResult.txt"));
            int i = 1;
            while (i <= 10) {
                writer.append("Hi for the " + i + ". time \n");
                i++;
            }
            writer.flush();
            writer.close();
        } catch (IOException ex) {
            Logger.getLogger(ExceptionHandling.class.getName()).log(Level.SEVERE, null, ex);
        }

        //try-with-resource, takes care of closing resources
        try (FileWriter writer = new FileWriter(new File("outputResultBye.txt"))) {
            int i = 1;
            while (i <= 10) {
                writer.append("Bye for the " + i + ". time \n");
                i++;
            }
            writer.flush();
        } catch (IOException ex) {
            Logger.getLogger(ExceptionHandling.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void getFolderNameAndStoreFiles(String folderName) throws SQLException, IncorrectLengthException {
        if (folderName.length() >= 3) {
            findIfNewFilesAndStore(folderName);
        } else {
            throw new IncorrectLengthException("Folder name is too short");
        }
    }

    public void findIfNewFilesAndStore(String folderName) throws SQLException {
        if (hasNewFiles(folderName)) {
            try {
                getFileContentAndStoreToDB();
            } catch (IOException ex) {
                System.out.println("Reading file failed: " + ex.getMessage());
            } catch (SQLException ex) {
                System.out.println("Creating DB connection failed: " + ex.getMessage());
                throw ex;
            }
        }
    }

    public void getFileContentAndStoreToDB() throws FileNotFoundException, IOException, SQLException {
        String dbUrl = "jdbc:mysql://localhost:3306/";
        Properties connectionProps = new Properties();
        connectionProps.put("user", "kasutajaMari");
        connectionProps.put("password", "kasutajaMariParool");
        try (FileReader reader = new FileReader(new File("notExistingFile.txt"));
                Connection connection = DriverManager.getConnection(dbUrl, connectionProps)) {
            char[] fileContent = new char[500];
            reader.read(fileContent);

            String sqlQuery = "INSERT INTO FEEDBACK (feedbackText) VALUES ('" + Arrays.toString(fileContent) + "');";
            Statement statement = connection.createStatement();
            statement.executeQuery(sqlQuery);
        }
    }

    private boolean hasNewFiles(String folderName) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
