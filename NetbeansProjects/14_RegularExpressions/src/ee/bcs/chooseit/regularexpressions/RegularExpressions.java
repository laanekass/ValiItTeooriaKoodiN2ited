package ee.bcs.chooseit.regularexpressions;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author heleen
 */
public class RegularExpressions {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String stringWithPattern = "This is a string wher 'magic' is looked for";
        Pattern magicPattern = Pattern.compile("magic|horror");

        Matcher matcher = magicPattern.matcher(stringWithPattern);

        if (matcher.find()) {
            //group() return what was found 
            System.out.println(matcher.group() + " is found :)");
        } else {
            System.out.println("There is no magic :(");
        }

        Pattern horrorPattern = Pattern.compile("horror");
        matcher = horrorPattern.matcher(stringWithPattern);

        if (matcher.find()) {
            System.out.println(matcher.group() + " is found :S");
        } else {
            //pattern() return pattern, that was looked for
            System.out.println("There is no " + matcher.pattern() + " :)");
        }

        String farSentence = "Long long time ago, in a land far, so far away...";
        Pattern farFarGreedyPattern = Pattern.compile("ago.*far");        
        matcher = farFarGreedyPattern.matcher(farSentence);
        if (matcher.find()) {
            System.out.println(matcher.group());
        }
        
        Pattern farFarOptimalPattern = Pattern.compile("ago.*?far");
        matcher = farFarOptimalPattern.matcher(farSentence);
        if (matcher.find()) {
            System.out.println(matcher.group());
        }
    }

}
