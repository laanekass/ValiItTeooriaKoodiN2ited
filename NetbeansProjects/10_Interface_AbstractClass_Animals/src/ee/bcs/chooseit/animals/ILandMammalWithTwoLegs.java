/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ee.bcs.chooseit.animals;

/**
 *
 * @author heleen
 */
public interface ILandMammalWithTwoLegs {
    void makeStepWithLeg(Leg leg);
}
