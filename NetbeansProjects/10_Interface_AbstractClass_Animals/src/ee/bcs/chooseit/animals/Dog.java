/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ee.bcs.chooseit.animals;

/**
 *
 * @author heleen
 */
public class Dog extends Mammal implements ILandMammal, ILandMammalWithFourLegs{
    boolean isStanding = false;
    
    @Override
    public void moveAhead() {
        if (!isStanding) {
            standUp();
        } 
        makeStepWithDiagonalLegs(Leg.LEFT, Leg.RIGHT);
        makeStepWithDiagonalLegs(Leg.RIGHT, Leg.LEFT);
    }

    @Override
    public void standUp() {
        System.out.println("Dog stood up.");
    }

    @Override
    public void makeStepWithDiagonalLegs(Leg frontLeg, Leg backLeg) {
        System.out.println("Dog made step ahead with " + frontLeg.toString() +" front leg and " + backLeg + " back leg."); 
    }    
}
