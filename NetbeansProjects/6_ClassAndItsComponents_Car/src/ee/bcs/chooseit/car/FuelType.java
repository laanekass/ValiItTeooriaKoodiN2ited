package ee.bcs.chooseit.car;

/**
 * Defines fuel type used for engine
 * @author heleen
 *
 */
public enum FuelType {
	DIESEL, PETROL;
}