/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ee.bcs.koolitus.restservice.service;

import ee.bcs.koolitus.restservice.entity.Employee;
import ee.bcs.koolitus.restservice.util.Gender;
import java.util.ArrayList;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;

/**
 *
 * @author heleen
 */
@Path("employees")
@RequestScoped
public class EmployeesResource {

    static List<Employee> employees = new ArrayList<>();

    @Context
    private UriInfo context;

    public EmployeesResource() {
        employees.add(new Employee("test", "testLats", Gender.MALE));
    }

    @GET
    @Produces("application/json")
    public List<Employee> getEmployeeList() {

        return employees;
    }

    @GET
    @Path("{id : \\d+}")
    @Produces("application/json")
    public Employee getEmployee(@PathParam("id") String id) {
        Employee employee = new Employee();
        return employee;
    }

    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public Employee addEmployee(Employee employee) {
        return employee;
    }
}
