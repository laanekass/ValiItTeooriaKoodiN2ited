/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ee.bcs.chooseit;

/**
 *
 * @author heleen
 */
public class StringDemo {

    /**
     * @param args - the command line arguments
     */
    public static void main(String[] args) {
        String singleWorldHello = "Hello ";
        String sentenceHowDoYouDo = "How do you do?";
        final char EXCLAMATION_MARK = '!';
        System.out.println(singleWorldHello);
        System.out.println(sentenceHowDoYouDo);
        // word + character
        System.out.println(singleWorldHello + EXCLAMATION_MARK);
        // word + sentence
        System.out.println(singleWorldHello.concat(sentenceHowDoYouDo));
        // Following row gives error, as concat doesn't approve simple types
        // System.out.println(singleWorldHello.concat(EXCLAMATION_MARK));
        System.out.println(singleWorldHello.concat(Character.toString(EXCLAMATION_MARK)));
        // you can concat numbers
        System.out.println(singleWorldHello + 1);
        System.out.println(singleWorldHello + 1.5);
        System.out.println("There are " + sentenceHowDoYouDo.length() + " characters in the sentence 'How do you do?'!");
        System.out.println("Make all characters to lower case: " + sentenceHowDoYouDo.toLowerCase());
        System.out.println("Make all characters to upper case: " + sentenceHowDoYouDo.toUpperCase());

        System.out.println("Cutting part of sentence: " + sentenceHowDoYouDo.substring(7, 10));
    }
}