package ee.bcs.chooseit;

/**
 *
 * @author heleen
 */
public class CompareStrings {

    public static boolean compareWithEquals(String firstString, String secondString) {
        // "\n" creates line break
        System.out.print("First string = '" + firstString + "'; Second string = '" + secondString + "'\n");
        return firstString.equals(secondString);
    }

    public static boolean compareWithEqualsIgnoreCase(String firstString, String secondString) {
        System.out.print("First string = '" + firstString + "'; Second string = '" + secondString + "'\n");
        return firstString.equalsIgnoreCase(secondString);
    }

    /**
     * Don't do like this
     *
     * @param firstString
     * @param secondString
     * @return
     */
    public static boolean compareUsingEqualsSign(String firstString, String secondString) {
        System.out.print("First string = '" + firstString + "'; Second string = '" + secondString + "'\n");
        // bad code example
        return firstString == secondString;
    }

    /**
     * 
     * @param args
     *            - the command line arguments
     */
    public static void main(String[] args) {
        System.out.println("Compares two Strings: " + compareWithEquals("is a word", "is a word"));
        /*
         * trim function removes spaces from the beginning and end of the String
         * In following example two strings by content are similar, but they are
         * located on different memory slots Equals-sign compares memory
         * addresses, therefore is result false
         */
        System.out.println("Compares addresses: " + compareUsingEqualsSign("is a word", "is a word ".trim()));
        System.out.println(
                        "While comparing checks also lower and upper cases: " + compareWithEquals("Is a word", "is a word"));
        System.out.println("While comparing ignores upper and lower cases: "
                        + compareWithEqualsIgnoreCase("Is a word", "is a word"));
    }
}