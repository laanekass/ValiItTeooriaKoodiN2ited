/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ee.bcs.koolitus.unittestdemo;

import java.math.BigDecimal;

/**
 *
 * @author heleen
 */
public class Salary {

    private static final BigDecimal TAX_FREE_AMOUNT = BigDecimal.valueOf(180);
    private final BigDecimal salaryGrossWage;

    public Salary(BigDecimal salaryGrossWage) {
        this.salaryGrossWage = salaryGrossWage;
    }

    public BigDecimal getSalaryGrossWage() {
        return this.salaryGrossWage;
    }

    public BigDecimal getSalaryNetWage() {
        return (salaryGrossWage.compareTo(TAX_FREE_AMOUNT) > 0) ? (salaryGrossWage.subtract(TAX_FREE_AMOUNT).multiply(BigDecimal.valueOf(0.8))) : (salaryGrossWage);
    }

}
