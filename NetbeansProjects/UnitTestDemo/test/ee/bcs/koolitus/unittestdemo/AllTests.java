/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ee.bcs.koolitus.unittestdemo;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

/**
 *
 * @author heleen
 */
@RunWith(Suite.class)
@SuiteClasses({
    SalaryTest.class,
    EmployeeTest.class
})
public class AllTests {

}
