/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ee.bcs.chooseit.employee;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author heleen
 */
public class Manager extends Employee {

    private String department;

    private final List<Integer> employeesInDepartment = new ArrayList<>();

    public Manager(int age, String name, BigDecimal salary, String department) {
        super(age, name, salary);
        this.department = department;
    }

    public String getDepartment() {
        return department;
    }

    public Manager setDepartment(String department) {
        this.department = department;
        return this;
    }

    public List<Integer> getEmployeesInDepartment() {
        return this.employeesInDepartment;
    }

    public Manager addEmployeeIntoManagerDepartment(Employee employee) {
        this.employeesInDepartment.add(employee.getId());
        return this;
    }

    public Manager removeEmployeeFromManagerDepartment(Employee employee) {
        for (int employeeId : employeesInDepartment) {
            if (employeeId == employee.getId()) {
                this.employeesInDepartment.remove(employeeId);
            }
        }
        //sama lambdaga
        /**
         * employeesInDepartment.stream().filter((employeeId) -> (employeeId ==
         * employee.getId())).forEachOrdered((employeeId) -> { this.employeesInDepartment.remove(employeeId);
        });
         */
        return this;
    }

    @Override
    public String toString() {
        return "Manager{name=" + getName() + ", department=" + department + ", employeesInDepartment="
                + employeesInDepartment + '}';
    }
}
