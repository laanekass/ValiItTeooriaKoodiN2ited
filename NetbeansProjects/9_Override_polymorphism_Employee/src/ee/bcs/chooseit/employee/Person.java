package ee.bcs.chooseit.employee;

/**
 *
 * @author heleen
 */
public class Person {

    private int age;
    private int id;
    private static int counter = 1;

    public Person(int age) {
        this.id = counter++;
        this.age = age;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAge() {
        return this.age;
    }

    public Person setAge(int age) {
        this.age = age;
        return this;
    }
}
