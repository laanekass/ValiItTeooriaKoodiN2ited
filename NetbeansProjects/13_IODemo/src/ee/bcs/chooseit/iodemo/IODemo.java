package ee.bcs.chooseit.iodemo;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;

/**
 *
 * @author heleen
 */
public class IODemo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        FileSystem fileSystem = FileSystems.getDefault();
        Path ioDemoFilePath = fileSystem.getPath("resources/IODemoFile.txt");
        System.out.println("File name is " + ioDemoFilePath.getFileName());
        System.out.println("File parent is " + ioDemoFilePath.getParent());
        System.out.println("File root is " + ioDemoFilePath.getRoot());
        System.out.println("Is path absolute?: " + ioDemoFilePath.isAbsolute());

        try (FileInputStream inputStream = new FileInputStream("resources/IODemoFile.txt");
                FileOutputStream outputWithAppendStream = new FileOutputStream("resources/IODemoFile.txt", true);
                FileOutputStream outputWithoutAppendStream = new FileOutputStream("resources/IODemoOutFileStream.txt")) {
            byte[] b = new byte[inputStream.available()];
            System.out.println("Created array for " + b.length + " bytes");
            while (inputStream.read(b, 0, b.length) != -1) {
                String test = new String(b);
                System.out.println(test);
            }
            outputWithAppendStream.write(b);
            outputWithoutAppendStream.write(b);
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

        try (BufferedReader reader = Files.newBufferedReader(ioDemoFilePath)) {
            reader.lines().forEach(line -> {
                System.out.println(line);
            });
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

        try (OutputStream outStream = new BufferedOutputStream(Files.newOutputStream(ioDemoFilePath, StandardOpenOption.APPEND))) {
            outStream.write("\ntest".getBytes());
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

        Path ioDemoOutFilePath = fileSystem.getPath("resources/IODemoOutFile.txt");
        try (BufferedWriter writer = Files.newBufferedWriter(ioDemoOutFilePath)) {
            writer.write("uue faili esimene rida\n");
            writer.write("uue faili teine rida\n");
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
}
